package com.homework.currencyCalculator.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.homework.currencyCalculator.entity.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Integer> {
	
	public List<Currency> findAllByOrderByIdAsc();

}
