package com.homework.currencyCalculator.dao;

import java.util.List;  

import org.springframework.data.jpa.repository.JpaRepository;

import com.homework.currencyCalculator.entity.CombinedCurrencies;

public interface CombinedCurrenciesRepository extends JpaRepository<CombinedCurrencies, Integer> {
	
	public List<CombinedCurrencies> findAllByOrderByIdAsc();

}
