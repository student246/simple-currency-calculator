package com.homework.currencyCalculator.service;

import java.util.List; 

import com.homework.currencyCalculator.entity.ScrapedCurrency;

public interface ScraperService {
	
	public List<ScrapedCurrency> getCurrencies();
	
	public void chechAvailableCombinedCurrencies(List<ScrapedCurrency> scrapedCurrency);
	
	public List<ScrapedCurrency> findAll();
	
	public int existingScrapedCurrency(String key);
}
