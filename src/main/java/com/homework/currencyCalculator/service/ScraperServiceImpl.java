package com.homework.currencyCalculator.service;

import java.io.IOException;    
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.homework.currencyCalculator.dao.CombinedCurrenciesRepository;
import com.homework.currencyCalculator.dao.ScrapedCurrencyRepository;
import com.homework.currencyCalculator.entity.CombinedCurrencies;
import com.homework.currencyCalculator.entity.ScrapedCurrency;

@Service
public class ScraperServiceImpl implements ScraperService {
	
	private ScrapedCurrencyRepository scrapedCurrencyRepository;
	private CombinedCurrenciesRepository combinedCurrenciesRepository;
	
	public ScraperServiceImpl(ScrapedCurrencyRepository theScrapedCurrencyRepository, CombinedCurrenciesRepository theCombinedCurrenciesRepository) {
		scrapedCurrencyRepository = theScrapedCurrencyRepository;
		combinedCurrenciesRepository = theCombinedCurrenciesRepository;
	}
	
	@Value("#{'${website.url}'}")
	private String url;
	
	@Override
	public List<ScrapedCurrency> getCurrencies() {

		List<ScrapedCurrency> scrapedCurrencies = new ArrayList<ScrapedCurrency>();
		
		extractDataFromWebsite(scrapedCurrencies, url);
		
		return scrapedCurrencies;
	}
	
	private void extractDataFromWebsite(List<ScrapedCurrency> scrapedCurrencies, String url) {
		
		try {
			
			Document document = Jsoup.connect(url).cookie("userLanguage=EN", "path=/").get();
			
			for (Element row : document.select("div.table_box tr")) {
				
				ScrapedCurrency scrapedCurrency = new ScrapedCurrency();
				
				if (row.select("td.center:nth-of-type(2)").text().equals("")) {
					continue;
				} else {
					final String name = row.select("td.first").text();
					final String code = row.select("td.center:nth-of-type(2)").text();
					final String value = row.select("td.right").text();
					final String sellPrice = row.select("td.center:nth-of-type(4)").text();
					final String sellBackPrice = row.select("td.center.last").text();
					
					scrapedCurrency.setCurrencyName(name);
					scrapedCurrency.setCurrencyCode(code);
					
					BigDecimal tempValue = new BigDecimal(0);
					BigDecimal tempSellPrice = new BigDecimal(0);
					
					BigDecimal temp = new BigDecimal(0);
					
					if (sellBackPrice.isEmpty() || sellBackPrice == null) {
						temp = new BigDecimal("0.0");
					} else {
						temp = new BigDecimal(sellBackPrice);
					}
					
					if (sellPrice.isEmpty() || sellPrice == null) {
						tempSellPrice = new BigDecimal("0.0");
					} else {
						tempSellPrice = new BigDecimal(sellPrice);
					}
					
					if (value.isEmpty() || value == null) {
						tempValue = new BigDecimal("0");
					} else {
						tempValue = new BigDecimal(value);
					}
					
					scrapedCurrency.setUnitValue(tempValue);
					scrapedCurrency.setUnitPrice(tempSellPrice);
					scrapedCurrency.setUnitSellPrice(temp);
					scrapedCurrency.setDateTime(LocalDateTime.now(ZoneOffset.ofHours(+6)));
					
					save(scrapedCurrency);
					
					scrapedCurrencies.add(scrapedCurrency);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void chechAvailableCombinedCurrencies(List<ScrapedCurrency> scrapedCurrency) {
		scrapedCurrency.sort(Comparator.comparing(ScrapedCurrency::getCurrencyCode).thenComparing(ScrapedCurrency::getDateTime));
		Map<String, ScrapedCurrency> scrapedCurrencyMap = scrapedCurrency.stream()
				.collect(Collectors.toMap(ScrapedCurrency::getCurrencyCode, data -> data , (data1, data2) -> data2));
		for (Map.Entry<String, ScrapedCurrency> entry : scrapedCurrencyMap.entrySet()) {
			String key = entry.getKey();
			ScrapedCurrency val = entry.getValue();
			System.out.println(key + "---" + val);
			if (existingScrapedCurrency(key) != -1) {
				int tempId = existingScrapedCurrency(key);
				CombinedCurrencies tempCurrency = combinedCurrenciesRepository.getById(tempId);
				tempCurrency.setUnitValue(val.getUnitValue());
				tempCurrency.setUnitPrice(val.getUnitPrice());
				tempCurrency.setUnitSellPrice(val.getUnitSellPrice());
				tempCurrency.setDateTime(val.getDateTime());
				combinedCurrenciesRepository.save(tempCurrency);
			} else {
				CombinedCurrencies combined = new CombinedCurrencies();
				combined.setCurrencyName(val.getCurrencyName());
				combined.setCurrencyCode(val.getCurrencyCode());
				combined.setUnitValue(val.getUnitValue());
				combined.setUnitPrice(val.getUnitPrice());
				combined.setUnitSellPrice(val.getUnitSellPrice());
				combined.setDateTime(val.getDateTime());
				combinedCurrenciesRepository.save(combined);
			}
			
		}
	}
	
	public int existingScrapedCurrency(String key) {
		List<CombinedCurrencies> combinedList = combinedCurrenciesRepository.findAll();
		
		for (CombinedCurrencies combinedCurrencies : combinedList) {
			if (key.equals(combinedCurrencies.getCurrencyCode())) {
				return combinedCurrencies.getId();
			}
		}
		return -1;
	}
	
	private void save(ScrapedCurrency theScrapedCurrency) {
		scrapedCurrencyRepository.save(theScrapedCurrency);
	}

	@Override
	public List<ScrapedCurrency> findAll() {
		return scrapedCurrencyRepository.findAll();
	}
}
