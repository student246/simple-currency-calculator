package com.homework.currencyCalculator.service;

import java.math.BigDecimal;

public interface CurrencyExchange {
	
	public BigDecimal currencyExchange(BigDecimal ammount, int fromCurrency, int currencyTo);
	
	public String currencyResult(int currencyTo, BigDecimal result);
	
	public String currencyAmmount(int fromCurrency, BigDecimal result);

}
