package com.homework.currencyCalculator.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name="scraped_currencies")
public class ScrapedCurrency {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@NotNull(message="is required")
	@NotEmpty(message="is required")
	@Column(name = "currency_name")
	private String currencyName;
	
	@Pattern(regexp="^[A-Z]{3}", message="only 3 chars are allowed")
	@Column(name = "currency_code")
	private String currencyCode;
	
	@Digits(integer = 10, fraction = 0, message = "Only integers numbers are allowed!")
	@Column(name = "unit_value")
	private BigDecimal unitValue;
	
	@Digits(integer = 10, fraction = 6, message = "Only integer numbers between 1 and 10 digits and decimal precission up to 6 digits are wllowed!")
	@Column(name = "unit_price")
	private BigDecimal unitPrice;
	
	@Digits(integer = 10, fraction = 6, message = "Only integer numbers between 1 and 10 digits and decimal precission up to 6 digits are wllowed!")
	@Column(name = "unit_sell_price")
	private BigDecimal unitSellPrice;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Column(name = "date_time")
	private LocalDateTime dateTime;
	
	public ScrapedCurrency() {
		
	}

	public ScrapedCurrency(int id, String currencyName, String currencyCode, BigDecimal unitValue, BigDecimal unitPrice, BigDecimal unitSell, LocalDateTime dateTime) {
		this.id = id;
		this.currencyName = currencyName;
		this.currencyCode = currencyCode;
		this.unitValue = unitValue;
		this.unitPrice = unitPrice;
		this.unitSellPrice = unitSell;
		this.dateTime = dateTime;
	}
	
	public ScrapedCurrency(String currencyName, String currencyCode, BigDecimal unitValue, BigDecimal unitPrice, BigDecimal unitSell, LocalDateTime dateTime) {
		this.currencyName = currencyName;
		this.currencyCode = currencyCode;
		this.unitValue = unitValue;
		this.unitPrice = unitPrice;
		this.unitSellPrice = unitSell;
		this.dateTime = dateTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(BigDecimal unitValue) {
		this.unitValue = unitValue;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getUnitSellPrice() {
		return unitSellPrice;
	}

	public void setUnitSellPrice(BigDecimal unitSellPrice) {
		this.unitSellPrice = unitSellPrice;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public String toString() {
		return "Currency [id=" + id + ", currencyName=" + currencyName + ", currencyCode=" + currencyCode
				+ ", unitValue=" + unitValue + ", unitPrice=" + unitPrice + ", unitSell=" + unitSellPrice + ", dateTime=" + dateTime + "]";
	}

}
