package com.homework.currencyCalculator.controller;

import java.util.List; 

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.homework.currencyCalculator.dao.CombinedCurrenciesRepository;
import com.homework.currencyCalculator.entity.CombinedCurrencies;

@Controller
public class LoginController {

private CombinedCurrenciesRepository combinedCurrenciesRepository;
	
	public LoginController(CombinedCurrenciesRepository theCombinedCurrenciesRepository) {
		combinedCurrenciesRepository = theCombinedCurrenciesRepository;
	}
	
	@GetMapping("/simple-login")
	public String simpleLogin() {
		
		return "calculator-simple-login";
	}
	
	@GetMapping("/success")
	public String loginRedirect(Model theModel, Authentication authentication) {
		if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
			List<CombinedCurrencies> theCurrencies = combinedCurrenciesRepository.findAllByOrderByIdAsc();
			
			theModel.addAttribute("currencies", theCurrencies);
			
			return "admin/edit-currencies";
		} else {
			return "redirect:/currencies/list";
		}
	}
	
	@GetMapping("logout")
	public String logoutRedirect() {
		
		return "redirect:/currencies/list";
	}
	
}
