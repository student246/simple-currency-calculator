-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: currency_list
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.21.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `currencies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_value` int DEFAULT NULL,
  `unit_price` float DEFAULT NULL,
  `unit_sell_price` float DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (66,'Bulgarian lev','BGN',10,1,1,'2022-03-26 09:48:50'),(67,'Bitcoin','BTC',1,60000,0.00158,'2022-03-26 22:48:04'),(68,'Ethereum','ETH',1,5589.98,0.00058,'2022-03-26 22:49:04'),(70,'PeshoCoin','PEC',1,0.0001,10000,'2022-04-04 17:39:20');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies_combined`
--

DROP TABLE IF EXISTS `currencies_combined`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `currencies_combined` (
  `id` int NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(45) DEFAULT NULL,
  `currency_code` varchar(3) DEFAULT NULL,
  `unit_value` int DEFAULT NULL,
  `unit_price` float DEFAULT NULL,
  `unit_sell_price` float DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1202 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies_combined`
--

LOCK TABLES `currencies_combined` WRITE;
/*!40000 ALTER TABLE `currencies_combined` DISABLE KEYS */;
INSERT INTO `currencies_combined` VALUES (1163,'Bitcoin','BTC',1,60000,0.00158,'2022-03-26 22:48:04'),(1164,'Ethereum','ETH',1,5589.98,0.00058,'2022-03-26 22:49:04'),(1165,'Bulgarian lev','BGN',10,1,1,'2022-03-26 09:48:50'),(1169,'Mexican Peso','MXN',100,8.96347,11.1564,'2022-04-04 17:20:58'),(1170,'South African Rand','ZAR',10,1.21513,8.22957,'2022-04-04 17:20:58'),(1171,'Indian Rupee','INR',100,2.35308,42.4975,'2022-04-04 17:20:58'),(1172,'Gold price (per troy ounce)','XAU',1,3435.55,0,'2022-04-04 17:20:58'),(1173,'Thai Baht','THB',100,5.30121,18.8636,'2022-04-04 17:20:58'),(1174,'Chinese Yuan Renminbi','CNY',10,2.79301,3.58037,'2022-04-04 17:20:58'),(1175,'Australian Dollar','AUD',1,1.33495,0.749092,'2022-04-04 17:20:58'),(1176,'South Korean Won','KRW',1000,1.46131,684.317,'2022-04-04 17:20:58'),(1177,'New Israel Shekel','ILS',10,5.53871,1.80547,'2022-04-04 17:20:58'),(1178,'Japanese Yen','JPY',100,1.4479,69.0655,'2022-04-04 17:20:58'),(1179,'Polish Zloty','PLN',10,4.21742,2.37112,'2022-04-04 17:20:58'),(1180,'British Pound','GBP',1,2.33142,0.428923,'2022-04-04 17:20:58'),(1181,'Indonesian Rupiah','IDR',10000,1.23913,8070.18,'2022-04-04 17:20:58'),(1182,'Hungarian Forint','HUF',1000,5.2982,188.743,'2022-04-04 17:20:58'),(1183,'Philippine Peso','PHP',100,3.46036,28.8987,'2022-04-04 17:20:58'),(1184,'Turkish Lira','TRY',10,1.20857,8.27424,'2022-04-04 17:20:58'),(1185,'Russian Rouble *','RUB',0,0,0,'2022-04-04 17:20:58'),(1186,'Icelandic Krona','ISK',100,1.37929,72.5011,'2022-04-04 17:20:58'),(1187,'Hong Kong Dollar','HKD',10,2.26826,4.40867,'2022-04-04 17:20:58'),(1188,'Danish Krone','DKK',10,2.62933,3.80325,'2022-04-04 17:20:58'),(1189,'US Dollar','USD',1,1.77722,0.562677,'2022-04-04 17:20:58'),(1190,'Malaysian Ringgit','MYR',10,4.21243,2.37393,'2022-04-04 17:20:58'),(1191,'Canadian Dollar','CAD',1,1.42253,0.702973,'2022-04-04 17:20:58'),(1192,'Norwegian Krone','NOK',10,2.04823,4.88226,'2022-04-04 17:20:58'),(1193,'Romanian Leu','RON',10,3.95661,2.52742,'2022-04-04 17:20:58'),(1194,'Singaporean Dollar','SGD',1,1.3093,0.763767,'2022-04-04 17:20:58'),(1195,'Czech Koruna','CZK',100,8.04206,12.4346,'2022-04-04 17:20:58'),(1196,'Swedish Krona','SEK',10,1.88334,5.30972,'2022-04-04 17:20:58'),(1197,'New Zealand Dollar','NZD',1,1.23318,0.810912,'2022-04-04 17:20:58'),(1198,'Brazilian Real','BRL',10,3.82282,2.61587,'2022-04-04 17:20:58'),(1199,'Croatian Kuna','HRK',10,2.59205,3.85795,'2022-04-04 17:20:58'),(1200,'Swiss Franc','CHF',1,1.91692,0.52167,'2022-04-04 17:20:58'),(1201,'PeshoCoin','PEC',1,0.0001,10000,'2022-04-04 17:39:20');
/*!40000 ALTER TABLE `currencies_combined` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scraped_currencies`
--

DROP TABLE IF EXISTS `scraped_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scraped_currencies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(45) DEFAULT NULL,
  `currency_code` varchar(3) DEFAULT NULL,
  `unit_value` int DEFAULT NULL,
  `unit_price` float DEFAULT NULL,
  `unit_sell_price` float DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2826 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scraped_currencies`
--

LOCK TABLES `scraped_currencies` WRITE;
/*!40000 ALTER TABLE `scraped_currencies` DISABLE KEYS */;
INSERT INTO `scraped_currencies` VALUES (2762,'Australian Dollar','AUD',1,1.33086,0.751394,'2022-04-04 17:08:27'),(2763,'Brazilian Real','BRL',10,3.74766,2.66833,'2022-04-04 17:08:27'),(2764,'Canadian Dollar','CAD',1,1.41675,0.705841,'2022-04-04 17:08:27'),(2765,'Swiss Franc','CHF',1,1.91429,0.522387,'2022-04-04 17:08:27'),(2766,'Chinese Yuan Renminbi','CNY',10,2.78168,3.59495,'2022-04-04 17:08:27'),(2767,'Czech Koruna','CZK',100,8.02359,12.4632,'2022-04-04 17:08:27'),(2768,'Danish Krone','DKK',10,2.62923,3.80339,'2022-04-04 17:08:27'),(2769,'British Pound','GBP',1,2.32436,0.430226,'2022-04-04 17:08:27'),(2770,'Hong Kong Dollar','HKD',10,2.25857,4.42758,'2022-04-04 17:08:27'),(2771,'Croatian Kuna','HRK',10,2.58451,3.86921,'2022-04-04 17:08:27'),(2772,'Hungarian Forint','HUF',1000,5.31302,188.217,'2022-04-04 17:08:27'),(2773,'Indonesian Rupiah','IDR',10000,1.23105,8123.15,'2022-04-04 17:08:27'),(2774,'New Israel Shekel','ILS',10,5.53824,1.80563,'2022-04-04 17:08:27'),(2775,'Indian Rupee','INR',100,2.32879,42.9408,'2022-04-04 17:08:27'),(2776,'Icelandic Krona','ISK',100,1.37735,72.6032,'2022-04-04 17:08:27'),(2777,'Japanese Yen','JPY',100,1.44502,69.2032,'2022-04-04 17:08:27'),(2778,'South Korean Won','KRW',1000,1.45349,687.999,'2022-04-04 17:08:27'),(2779,'Mexican Peso','MXN',100,8.92718,11.2017,'2022-04-04 17:08:27'),(2780,'Malaysian Ringgit','MYR',10,4.20301,2.37925,'2022-04-04 17:08:27'),(2781,'Norwegian Krone','NOK',10,2.02408,4.94052,'2022-04-04 17:08:27'),(2782,'New Zealand Dollar','NZD',1,1.22923,0.813517,'2022-04-04 17:08:27'),(2783,'Philippine Peso','PHP',100,3.42623,29.1866,'2022-04-04 17:08:27'),(2784,'Polish Zloty','PLN',10,4.21506,2.37245,'2022-04-04 17:08:27'),(2785,'Romanian Leu','RON',10,3.95501,2.52844,'2022-04-04 17:08:27'),(2786,'Russian Rouble *','RUB',0,0,0,'2022-04-04 17:08:27'),(2787,'Swedish Krona','SEK',10,1.89298,5.28268,'2022-04-04 17:08:27'),(2788,'Singaporean Dollar','SGD',1,1.30519,0.766172,'2022-04-04 17:08:27'),(2789,'Thai Baht','THB',100,5.29447,18.8876,'2022-04-04 17:08:27'),(2790,'Turkish Lira','TRY',10,1.20425,8.30392,'2022-04-04 17:08:27'),(2791,'US Dollar','USD',1,1.76966,0.56508,'2022-04-04 17:08:27'),(2792,'South African Rand','ZAR',10,1.20965,8.26685,'2022-04-04 17:08:27'),(2793,'Gold price (per troy ounce)','XAU',1,3415.84,0,'2022-04-04 17:08:27'),(2794,'Australian Dollar','AUD',1,1.33495,0.749092,'2022-04-04 17:20:58'),(2795,'Brazilian Real','BRL',10,3.82282,2.61587,'2022-04-04 17:20:58'),(2796,'Canadian Dollar','CAD',1,1.42253,0.702973,'2022-04-04 17:20:58'),(2797,'Swiss Franc','CHF',1,1.91692,0.52167,'2022-04-04 17:20:58'),(2798,'Chinese Yuan Renminbi','CNY',10,2.79301,3.58037,'2022-04-04 17:20:58'),(2799,'Czech Koruna','CZK',100,8.04206,12.4346,'2022-04-04 17:20:58'),(2800,'Danish Krone','DKK',10,2.62933,3.80325,'2022-04-04 17:20:58'),(2801,'British Pound','GBP',1,2.33142,0.428923,'2022-04-04 17:20:58'),(2802,'Hong Kong Dollar','HKD',10,2.26826,4.40867,'2022-04-04 17:20:58'),(2803,'Croatian Kuna','HRK',10,2.59205,3.85795,'2022-04-04 17:20:58'),(2804,'Hungarian Forint','HUF',1000,5.2982,188.743,'2022-04-04 17:20:58'),(2805,'Indonesian Rupiah','IDR',10000,1.23913,8070.18,'2022-04-04 17:20:58'),(2806,'New Israel Shekel','ILS',10,5.53871,1.80547,'2022-04-04 17:20:58'),(2807,'Indian Rupee','INR',100,2.35308,42.4975,'2022-04-04 17:20:58'),(2808,'Icelandic Krona','ISK',100,1.37929,72.5011,'2022-04-04 17:20:58'),(2809,'Japanese Yen','JPY',100,1.4479,69.0655,'2022-04-04 17:20:58'),(2810,'South Korean Won','KRW',1000,1.46131,684.317,'2022-04-04 17:20:58'),(2811,'Mexican Peso','MXN',100,8.96347,11.1564,'2022-04-04 17:20:58'),(2812,'Malaysian Ringgit','MYR',10,4.21243,2.37393,'2022-04-04 17:20:58'),(2813,'Norwegian Krone','NOK',10,2.04823,4.88226,'2022-04-04 17:20:58'),(2814,'New Zealand Dollar','NZD',1,1.23318,0.810912,'2022-04-04 17:20:58'),(2815,'Philippine Peso','PHP',100,3.46036,28.8987,'2022-04-04 17:20:58'),(2816,'Polish Zloty','PLN',10,4.21742,2.37112,'2022-04-04 17:20:58'),(2817,'Romanian Leu','RON',10,3.95661,2.52742,'2022-04-04 17:20:58'),(2818,'Russian Rouble *','RUB',0,0,0,'2022-04-04 17:20:58'),(2819,'Swedish Krona','SEK',10,1.88334,5.30972,'2022-04-04 17:20:58'),(2820,'Singaporean Dollar','SGD',1,1.3093,0.763767,'2022-04-04 17:20:58'),(2821,'Thai Baht','THB',100,5.30121,18.8636,'2022-04-04 17:20:58'),(2822,'Turkish Lira','TRY',10,1.20857,8.27424,'2022-04-04 17:20:58'),(2823,'US Dollar','USD',1,1.77722,0.562677,'2022-04-04 17:20:58'),(2824,'South African Rand','ZAR',10,1.21513,8.22957,'2022-04-04 17:20:58'),(2825,'Gold price (per troy ounce)','XAU',1,3435.55,0,'2022-04-04 17:20:58');
/*!40000 ALTER TABLE `scraped_currencies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05 13:49:40
-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: calculator_security_bcrypt
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.21.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `authorities_idx_1` (`username`,`authority`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES ('Alex','ROLE_ADMIN'),('Stefan','ROLE_ADMIN'),('Venko','ROLE_ADMIN');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` char(68) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('Alex','{bcrypt}$2a$10$AHCjFUaEWZtUBlOVVSB88ODl947eqlzGLWq4dZ/zaTVbTYYRk1Cvq',1),('Stefan','{bcrypt}$2a$10$AHCjFUaEWZtUBlOVVSB88ODl947eqlzGLWq4dZ/zaTVbTYYRk1Cvq',1),('Venko','{bcrypt}$2a$10$AHCjFUaEWZtUBlOVVSB88ODl947eqlzGLWq4dZ/zaTVbTYYRk1Cvq',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-05 13:49:40
