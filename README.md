Introduction
--------------------

This is a simple currency calculator, which can calculate the desired amount based on the latest currency exchange rates, taken directly from the HTML of the <a href="https://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm">Bulgarian National Bank</a>.

If you would like to checkout the working demo app, please visit this <a href="http://alextk.ml/currencies/list">link</a>.

The currencies are scraped from the HTML to a model and then saved into a database table which is holding each and every scraped currency entry. After that, the currencies are sorted by their currency code and by date and time and the latest updated currencies are then saved in a new database table which combines the latest scraped currencies and the data from the user input currencies.

By loging into the management panel, the user can add custom currency, to update the currently available currencies or to delete a currency from the database.
If you would like to checkout the management panel, please use the username:"Alex" with a password:"login_test", without the quotes.

Deployment
--------------------

If you would like to deploy the project on your own system, please install MySQL and then import the dump of the currency_list database, which is provided in resource directory of the project.
Later on, I'm planning to include the packaged jar file for 

Future improvements.
---------------------

I'm planning to implement a history function, in which the users can view the history of all the currencies which are scraped or manually created/updated by the admin panel.
